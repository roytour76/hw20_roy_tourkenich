﻿namespace EX1
{
    partial class Desk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.score = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // score
            // 
            this.score.AutoSize = true;
            this.score.Location = new System.Drawing.Point(111, 31);
            this.score.Name = "score";
            this.score.Size = new System.Drawing.Size(63, 13);
            this.score.TabIndex = 0;
            this.score.Text = "Your Score:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(809, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Opponert\'s Score:";
            // 
            // btt
            // 
            this.btt.AutoSize = true;
            this.btt.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btt.Location = new System.Drawing.Point(445, 31);
            this.btt.Name = "btt";
            this.btt.Size = new System.Drawing.Size(107, 52);
            this.btt.TabIndex = 2;
            this.btt.Text = "Retire";
            this.btt.UseVisualStyleBackColor = true;
            this.btt.Click += new System.EventHandler(this.button1_Click);
            // 
            // Desk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(1033, 504);
            this.Controls.Add(this.btt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.score);
            this.Name = "Desk";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Desk_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label score;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btt;
    }
}

