﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace EX1
{
    public partial class Desk : Form
    {
        public Desk()
        {
            InitializeComponent();
            
        }
        public void GenerateCard()
        {
            PictureBox[] PicsCards = new PictureBox[10];
            int i = 0, dist = 15;
                       
            for (i = 0; i < 10; i++)
            {
                PicsCards[i] = new PictureBox();
                PicsCards[i].Image = EX1.Properties.Resources.card_back_red;
                PicsCards[i].Location = new Point(dist, 300);
                PicsCards[i].Name = "pic" + i;
                PicsCards[i].Size = new Size(90, 114);
                PicsCards[i].SizeMode = PictureBoxSizeMode.StretchImage;

                dist += 100;
                PicsCards[i].TabIndex = 2;
                PicsCards[i].TabStop = false;
                Controls.Add(PicsCards[i]);
            }

        }

        private void Desk_Load(object sender, EventArgs e)
        {
            GenerateCard();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
    
}
